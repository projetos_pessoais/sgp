<?php

use Illuminate\Database\Seeder;

class ProjectTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        sgp\Entities\Project::truncate();
        factory(\sgp\Entities\Project::class , 10)->create();
    }
}
