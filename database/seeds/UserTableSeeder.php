<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        factory(\sgp\Entities\User::class)->create([
            'name' => 'Alexandre',
            'email' => 'alexandre@1linha.com.br',
            'password' => bcrypt(123456),
            'remember_token' => str_random(10),
        ]);

        factory(\sgp\Entities\User::class , 10)->create();
    }
}
