<?php

use Illuminate\Database\Seeder;

class ProjectNoteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        sgp\Entities\ProjectNote::truncate();
        factory(\sgp\Entities\ProjectNote::class , 50)->create();
    }
}
