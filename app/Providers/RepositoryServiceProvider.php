<?php

namespace sgp\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(\sgp\Repositories\ProjectRepository::class, \sgp\Repositories\ProjectRepositoryEloquent::class);
        $this->app->bind(\sgp\Repositories\ProjectNoteRepository::class, \sgp\Repositories\ProjectNoteRepositoryEloquent::class);
        //:end-bindings:
    }
}
