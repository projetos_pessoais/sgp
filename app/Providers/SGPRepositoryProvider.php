<?php

namespace sgp\Providers;

use Illuminate\Support\ServiceProvider;

class SGPRepositoryProvider extends ServiceProvider
{
    /**
     * Bootstrap the application Services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application Services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            \sgp\Repositories\ClientRepository::class,
            \sgp\Repositories\ClientRepositoryEloquent::class
        );

        $this->app->bind(
            \sgp\Repositories\ProjectRepository::class,
            \sgp\Repositories\ProjectRepositoryEloquent::class
        );

        $this->app->bind(
            \sgp\Repositories\ProjectNoteRepository::class,
            \sgp\Repositories\ProjectNoteRepositoryEloquent::class
        );
    }
}
