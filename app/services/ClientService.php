<?php
/**
 * Created by PhpStorm.
 * User: Alexandre Sanches
 * Date: 27/05/2016
 * Time: 13:20
 */

namespace sgp\Services;


use Illuminate\Contracts\Validation\ValidationException;
use Prettus\Validator\Exceptions\ValidatorException;
use sgp\Repositories\ClientRepository;
use sgp\Validators\ClientValidator;

class ClientService
{

    protected $repository;

    protected $validator;

    public function __construct(ClientRepository $repository, ClientValidator $validator)
    {
        $this->repository = $repository;
        $this->validator = $validator;
    }

    public function create(array $data)
    {
        // enviar email
        // disparar notificação
        // postar tweet
        // etc

        try{
            $this->validator->with($data)->passesOrFail();
            return $this->repository->create($data);
        } catch (ValidatorException $e) {
            return [
                'error' => true,
                'message' => $e->getMessageBag()
            ];
        }
    }

    public function update(array $data, $id){

        try{
            $this->validator->with($data)->passesOrFail();
            return $this->repository->update($data, $id);
        } catch (ValidatorException $e) {
            return [
                'error' => true,
                'message' => $e->getMessageBag()
            ];
        }
    }
}