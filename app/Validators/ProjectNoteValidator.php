<?php
/**
 * Created by PhpStorm.
 * User: Alexandre Sanches
 * Date: 27/05/2016
 * Time: 13:57
 */

namespace sgp\Validators;


use Prettus\Validator\LaravelValidator;

class ProjectNoteValidator extends LaravelValidator
{

    protected $rules = [
      'project_id' => 'required|integer',
        'title' => 'required',
        'note' => 'required',
    ];

}