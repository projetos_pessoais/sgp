<?php
/**
 * Created by PhpStorm.
 * User: Alexandre Sanches
 * Date: 27/05/2016
 * Time: 13:57
 */

namespace sgp\Validators;


use Prettus\Validator\LaravelValidator;

class ClientValidator extends LaravelValidator
{

    protected $rules = [
      'name' => 'required|max:150',
        'responsible' => 'required|max:255',
        'email' => 'required|email',
        'address' => 'required'
    ];

}