<?php
/**
 * Created by PhpStorm.
 * User: Alexandre Sanches
 * Date: 28/05/2016
 * Time: 10:24
 */

namespace sgp\OAuth;

use Illuminate\Support\Facades\Auth;

class Verifier
{
    public function verify($username, $password)
    {
        $credentials = [
            'email'    => $username,
            'password' => $password,
        ];

        if (Auth::once($credentials)) {
            return Auth::user()->id;
        }

        return false;
    }

}