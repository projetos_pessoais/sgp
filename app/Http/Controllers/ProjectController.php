<?php

namespace sgp\Http\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

use sgp\Entities\Client;
use sgp\Http\Requests;
use sgp\Repositories\ClientRepository;
use sgp\Repositories\ProjectRepository;
use sgp\Services\ClientService;
use sgp\Services\ProjectService;



class ProjectController extends Controller
{

    private $repository;
    /**
     * @var ClientService
     */
    private $service;

    public function __construct(ProjectRepository $repository, ProjectService $service) // ClientRepository interface registrada em config\app.php
    {
        $this->repository = $repository;
        $this->service = $service;
    }


    public function index(){

        return $this->repository->findWhere(['owner_id'=> \Authorizer::getResourceOwnerId()]);

    }

    public function store(Request $request)
    {
        return $this->service->create($request->all());
    }

    public function show($id)
    {

        if ($this->checkProjectPermissions($id)  == false) {
            return response('Access Forbidden');
        }


        return $this->repository->find($id);

    }

    public function destroy($id)
    {

        if ($this->checkProjectOwner($id) == false) {
            return response('Access Forbidden');
        }

        try
        {
            $this->repository->delete($id);
            return response()->json([
                'error'=> false,
                'message' => 'Cliente apagado com sucesso',
        ]);
        }

        catch (\Exception $e)
        {
            return response()->json([
                'error'=> true,
                'message' => 'Cliente não encontrado'
            ]);
        }
    }
    
    public function update(Request $request, $id)
    {
        if ($this->checkProjectPermissions($id) == false) {
            return response('Access Forbidden');
        }
        return $this->service->update($request->all(), $id);

    }

    public function checkProjectOwner($projectId)
    {
        $userId = \Authorizer::getResourceOwnerId();

        return ($this->repository->isOwner($projectId, $userId));

    }

    public function checkProjectMember($projectId)
    {
        $userId = \Authorizer::getResourceOwnerId();

        return ($this->repository->hasMember($projectId, $userId));

    }

    private function checkProjectPermissions($projectId)
    {

        if($this->checkProjectOwner($projectId) or $this->checkProjectMember($projectId) == true){
                return true;
        }

    }

}
