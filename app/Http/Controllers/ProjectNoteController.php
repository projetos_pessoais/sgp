<?php

namespace sgp\Http\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;


use sgp\Http\Requests;

use sgp\Repositories\ProjectNoteRepository;
use sgp\Services\ClientService;
use sgp\Services\ProjectNoteService;



class ProjectNoteController extends Controller
{

    private $repository;
    /**
     * @var ClientService
     */
    private $service;

    public function __construct(ProjectNoteRepository $repository, ProjectNoteService $service) // ClientRepository interface registrada em config\app.php
    {
        $this->repository = $repository;
        $this->service = $service;
    }


    public function index($id){
        return $this->repository->findWhere(['project_id' => $id]);
    }

    public function store(Request $request)
    {
        return $this->service->create($request->all());
    }

    public function show($id, $noteId){
        try
        {
            return $this->repository->findWhere(['project_id' => $id, 'id' => $noteId]);
        }
        catch (\Exception $e)
        {
            return response()->json([
                'error'=> true,
                'message' => 'Nota do projeto não encontrado'
            ]);
        }
    }

    public function destroy($id)
    {
        try
        {
            $this->repository->delete($id);
            return response()->json([
                'error'=> false,
                'message' => 'Nota do projeto apagado com sucesso',
        ]);
        }

        catch (\Exception $e)
        {
            return response()->json([
                'error'=> true,
                'message' => 'Cliente não encontrado'
            ]);
        }
    }
    
    public function update(Request $request, $id)
    {

        return $this->service->update($request->all(), $id);

    }


}
