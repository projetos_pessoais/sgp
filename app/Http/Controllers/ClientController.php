<?php

namespace sgp\Http\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

use sgp\Entities\Client;
use sgp\Http\Requests;
use sgp\Repositories\ClientRepository;
use sgp\Services\ClientService;


class ClientController extends Controller
{

    private $repository;
    /**
     * @var ClientService
     */
    private $service;

    public function __construct(ClientRepository $repository, ClientService $service) // ClientRepository interface registrada em config\app.php
    {
        $this->repository = $repository;
        $this->service = $service;
    }


    public function index(){
        return $this->repository->all();
    }

    public function store(Request $request)
    {
        return $this->service->create($request->all());
    }

    public function show($id){
        try
        {
            return $this->repository->find($id);
        }
        catch (\Exception $e)
        {
            return response()->json([
                'error'=> true,
                'message' => 'Cliente não encontrado'
            ]);
        }
    }

    public function destroy($id)
    {
        try
        {
            $this->repository->delete($id);
            return response()->json([
                'error'=> false,
                'message' => 'Cliente apagado com sucesso',
        ]);
        }

        catch (\Exception $e)
        {
            return response()->json([
                'error'=> true,
                'message' => 'Cliente não encontrado'
            ]);
        }
    }
    
    public function update(Request $request, $id)
    {

        return $this->service->update($request->all(), $id);

    }


}
