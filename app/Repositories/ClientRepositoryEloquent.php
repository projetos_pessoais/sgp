<?php
/**
 * Created by PhpStorm.
 * User: Alexandre Sanches
 * Date: 25/05/2016
 * Time: 16:12
 */

namespace sgp\Repositories;



use Prettus\Repository\Eloquent\BaseRepository;
use sgp\Entities\Client;


class ClientRepositoryEloquent extends BaseRepository implements ClientRepository
{

    public function model()
    {
        return Client::class;
    }

}
