<?php

namespace sgp\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ProjectNoteRepository
 * @package namespace sgp\Repositories;
 */
interface ProjectNoteRepository extends RepositoryInterface
{
    //
}
