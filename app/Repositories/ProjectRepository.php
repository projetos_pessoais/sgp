<?php

namespace sgp\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ProjectRepository
 * @package namespace sgp\Repositories;
 */
interface ProjectRepository extends RepositoryInterface
{
    //
}
