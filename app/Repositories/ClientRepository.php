<?php
/**
 * Created by PhpStorm.
 * User: Alexandre Sanches
 * Date: 25/05/2016
 * Time: 17:01
 */

namespace sgp\Repositories;


use Prettus\Repository\Contracts\RepositoryInterface;

interface ClientRepository extends RepositoryInterface
{
    
}