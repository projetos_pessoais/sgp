<?php
/**
 * Created by PhpStorm.
 * User: Alexandre Sanches
 * Date: 31/05/2016
 * Time: 09:24
 */

namespace sgp\Presenters;

use Prettus\Repository\Presenter\FractalPresenter;
use sgp\Transformers\ProjectTransformer;


class ProjectPresenter extends FractalPresenter
{

    public function getTransformer()
    {
        return new ProjectTransformer();
    }


}