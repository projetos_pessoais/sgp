<?php
/**
 * Created by PhpStorm.
 * User: Alexandre Sanches
 * Date: 27/05/2016
 * Time: 13:20
 */

namespace sgp\Services;



use Illuminate\Contracts\Filesystem\Factory as Storage;
use Prettus\Validator\Exceptions\ValidatorException;

use sgp\Repositories\ProjectRepository;
use sgp\Validators\ProjectValidator;

class ProjectService
{

    protected $repository;

    protected $validator;
    /**
     * @var Storage
     */
    private $storage;

    public function __construct(ProjectRepository $repository, ProjectValidator $validator, Storage $storage)
    {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->storage = $storage;
    }

    public function create(array $data)
    {
        // enviar email
        // disparar notificação
        // postar tweet
        // etc

        try{
            $this->validator->with($data)->passesOrFail();
            return $this->repository->create($data);
        } catch (ValidatorException $e) {
            return [
                'error' => true,
                'message' => $e->getMessageBag()
            ];
        }
    }

    public function update(array $data, $id){

        try{
            $this->validator->with($data)->passesOrFail();
            return $this->repository->update($data, $id);
        } catch (ValidatorException $e) {
            return [
                'error' => true,
                'message' => $e->getMessageBag()
            ];
        }
    }
    
    public function createFile(array $data) // name, description, extension, file
    {

        $project = $this->repository->skipPresenter()->find($data['project_id']);

        $projectFile =  $project->files()->create($data);

        $this->storage->put($projectFile->id .".". $data['extension'], $data['file']);


        
    }
}