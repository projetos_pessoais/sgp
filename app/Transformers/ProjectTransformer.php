<?php
/**
 * Created by PhpStorm.
 * User: Alexandre Sanches
 * Date: 31/05/2016
 * Time: 09:09
 */

namespace sgp\Transformers;

use League\Fractal\TransformerAbstract;
use sgp\Entities\Project;
use sgp\Transformers\ProjectMemberTransformer;


class ProjectTransformer extends TransformerAbstract
{

    protected $defaultIncludes = ['members'];

    public function transform(Project $project)
    {
        return [
            'project_id' => $project->id,
            'client_id' => $project->client_id,
            'owner_id' => $project->owner_id,
            'project' => $project->name,
            'description' => $project->description,
            'progress' => $project->progress,
            'status' => $project->status,
            'due_date' => $project->due_date,
        ];
    }

    public function includeMembers(Project $project)
    {
        return $this->collection($project->members, new ProjectMemberTransformer() );
    }

}