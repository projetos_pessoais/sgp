<?php
/**
 * Created by PhpStorm.
 * User: Alexandre Sanches
 * Date: 31/05/2016
 * Time: 09:09
 */

namespace sgp\Transformers;

use League\Fractal\TransformerAbstract;

use sgp\Entities\User;


class ProjectMemberTransformer extends TransformerAbstract
{

    public function transform(User $member)
    {
        return [
            'member_id' => $member->id,
            'nome' => $member->name
        ];
    }

}